app.controller('itemsController',function($scope,Sqlite,$state,editFactory)
{
        Sqlite.all("select * from item",function(err,row){
            console.log(row);
            var items=row;
            $scope.items=items;
        });
    $scope.deleteItem=function(item_number){
        console.log(item_number);
        var r = confirm("are you sure to want to delete "+item_number+" record");
        if(r==true) {
            Sqlite.run("delete from item where item_number='" +item_number+ "'", function (err, row) {
                console.log(err, row);
                $state.go('items', {}, { reload: true });
            })
        }
    }
    $scope.editItem=function(item_number){
        localStorage.setItem("edit", angular.toJson(item_number));
        $state.go("editItem");
}

});