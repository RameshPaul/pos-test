app.controller('customersController',function($scope,Sqlite,$state){
    Sqlite.all("select * from customer",function(err,row){
        console.log(err,row);
        $scope.customers=row;
    })
    $scope.delete=function(email){
        console.log(email);
        var r = confirm("are you sure to want to delete "+email+" record");
        if(r==true) {
            Sqlite.run("delete from customer where email='" + email + "'", function (err, row) {
                console.log(err, row);
                $state.go('customers', {}, { reload: true });
            })
        }
    }
    $scope.edit=function(email){
        console.log(email);

        localStorage.setItem("email", angular.toJson(email));
        $state.go("editCustomer");
    }
})