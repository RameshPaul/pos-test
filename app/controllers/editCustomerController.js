/**
 * Created by ajay on 23/11/16.
 */
app.controller('editCustomerController',function ($scope,$state,Sqlite){
    var email = angular.fromJson(localStorage.getItem("email"));
    console.log(email);
    Sqlite.each("select * from customer where email='"+email+"'",function(err,row){
        console.log(err,row);
        $scope.customer=row;
    })
    $scope.editCustomer=function(customer){
        var stmt = Sqlite.prepare("update customer SET first_name=?,last_name=?,email=?,phone_number=?,gender=?,leadsource=?,image=?,address1=?,address2=?,city=?,state=?,zip=?,country=?,comments=?,company_name=?,account=? where email=?");
        stmt.run(customer.first_name, customer.last_name, customer.email, customer.phone_number, customer.gender, customer.leadsource, customer.image, customer.address1, customer.address2, customer.city, customer.state, customer.zip, customer.country, customer.comments, customer.company_name, customer.account,email,function(err,row){
            if(err==null){
                $scope.success="'"+customer.first_name+"' customer edited successfully";
                alert("Customer updated Successfully");
                $state.go("customers");


            }
            else{
                $scope.err="email-id '"+customer.email+"' already existed please enter another email";
            }

        });

    }
    $scope.cancel=function(){
        $state.go("customers");
    }
});