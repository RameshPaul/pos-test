app.controller('editsuppliersController',function ($scope,$state,Sqlite){
    var email = angular.fromJson(localStorage.getItem("editSupplier"));
    console.log(email);
    Sqlite.each("select * from supplier where email='"+email+"'",function(err,row){
        console.log(err,row);
        $scope.supplier=row;
    })
    $scope.editsupplier=function(supplier){
        var stmt = Sqlite.prepare("update supplier SET companyname=?,first_name=?,last_name=?,email=?,phonenumber=?,adress1=?,adress2=?,city=?,state=?,zip=?,country=?,comments=?,account=? where email=?");
        stmt.run(supplier.companyname,supplier.first_name, supplier.last_name, supplier.email, supplier.phonenumber,supplier.adress1,supplier.adress2, supplier.city,supplier.state,supplier.zip,supplier.country,supplier.comments,supplier.account,email,function(err,row){
            if(err==null) {
                alert("supplier updated successfully");
                $state.go("suppliers");

            }
            else{
                $scope.err="supplier editing failed'" +supplier.email+ "' Email-id already existed please enter another Email-id";
            }
        });

    }
    $scope.cancel=function(){
        $state.go("suppliers");
    }
});