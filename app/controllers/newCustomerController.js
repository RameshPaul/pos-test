/**
 * Created by ajay on 21/11/16.
 */
app.controller('newCustomerController', function ($scope, $state, Sqlite) {
    $scope.save = function (customer) {
        console.log(customer);

            var stmt = Sqlite.prepare("INSERT into customer values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            stmt.run(customer.first_name, customer.last_name, customer.email, customer.phone_number, customer.gender, customer.leadsource, customer.image, customer.address1, customer.address2, customer.city, customer.state, customer.zip, customer.country, customer.comments, customer.company_name, customer.account,function(err,row){
            if(err==null) {
                alert("customer inserted successfully");
                $state.go("customers");
            }
            else{
                $scope.err="customer not inserted '"+customer.email+"' Email-id already existed please enter another Email-id";
            }
        });

    }
    $scope.cancel=function(){
        $state.go("customers");
    }
})