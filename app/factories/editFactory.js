app.factory('editFactory', function() {
    var edit = null;
    return {
        getJson: function () {
            return edit;
        },
        setJson: function (value) {
            edit = value;
        }
    }
})