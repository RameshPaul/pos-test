var app = angular.module('app', ['ui.router','ui.bootstrap']);

app.provider('Sqlite', function() {
    this.db = false;
    this.connect = function(config) {
        var sqlite3 = require('sqlite3').verbose();
        this.db = new sqlite3.Database(config.dbName);
    };
    this.$get = function ($rootScope) {
        return this.db;
    };
    return this;
});

app.config(function ($stateProvider, $urlRouterProvider,SqliteProvider) {
    SqliteProvider.connect({dbName:'admin.db'});
    $stateProvider
        .state('login', {
            url: '/',
            templateUrl: 'app/views/login-view.html',
            controller: 'loginController'
        })
        .state('registration', {
            url: '/registration',
            templateUrl: 'app/views/registrationView.html',
            controller: 'registrationController'
        })
        .state('forgotPassword', {
            url: '/forgotPassword',
            templateUrl: 'app/views/forgotPassword-view.html',
            controller: 'forgotPasswordController'

        })
        .state('home', {
            url: '/home',
            templateUrl: 'app/views/home-view.html',
            controller: 'homeController'
        })
        .state('profile', {
            url: '/profile',
            templateUrl: 'app/views/profile-view.html',
            controller: 'profileController'
        })
        .state('updatePassword', {
            url: '/updatePassword',
            templateUrl: 'app/views/updatePassword-view.html',
            controller: 'updatePasswordController'

        })
        .state('customers', {
            url: '/customers',
            templateUrl: 'app/views/customer-view.html',
            controller: "customersController"
        })
        .state('newCustomer', {
            url: '/newcustomer',
            templateUrl: 'app/views/newCustomer-view.html',
            controller: 'newCustomerController'
        })
        .state('suppliers', {
            url: '/suppliers',
            templateUrl: 'app/views/suppliers-view.html',
            controller: 'suppliersController'
        })
        .state('newsuppliers', {
            url: '/newsuppliers',
            templateUrl: 'app/views/newsuppliers-view.html',
            controller: 'newsuppliersController'

        })
        .state('editSuppliers', {
            url: '/editsuppliers',
            templateUrl: 'app/views/editsuppliers-view.html',
            controller: 'editsuppliersController'

        })
        .state('items', {
            url: '/items',
            templateUrl: 'app/views/items-view.html',
            controller: 'itemsController'
        })
        .state('item', {
            url: '/item',
            templateUrl: 'app/views/item-view.html',
            controller: 'itemController'
        })
        .state('editItem', {
            url: '/editItem',
            templateUrl: 'app/views/editItem-view.html',
            controller: 'editItemController'
        })
        .state('editCustomer',{
            url:'/editCustomer',
            templateUrl:'app/views/editCustomer-view.html',
            controller:'editCustomerController'
        })
        $urlRouterProvider.otherwise('/');
});

