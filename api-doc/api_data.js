define({ "api": [
  {
    "type": "get",
    "url": "/api/user/:id",
    "title": "04.Get user profile",
    "name": "Get_User",
    "group": "01_User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"user fetched successfully\",\n    \"data\":  {}\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/user/controllers/user.js",
    "groupTitle": "01_User"
  },
  {
    "type": "post",
    "url": "/api/login",
    "title": "02.Login user",
    "name": "Login_User",
    "group": "01_User",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"username\": \"abc123456@123.com\",\n    \"password\": \"Retail Store\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"user fetched successfully\",\n    \"data\": {\n\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/user/controllers/user.js",
    "groupTitle": "01_User"
  },
  {
    "type": "post",
    "url": "/api/register",
    "title": "01.Register new user",
    "name": "Register_User",
    "group": "01_User",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"first_name\": \"abc123456\",\n    \"email\": \"asdasd@sd.com\",\n    \"phone_number\": \"175445695\",\n    \"business_type\": \"Hotels\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"user registered successfully\",\n    \"data\": {\n\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/user/controllers/user.js",
    "groupTitle": "01_User"
  },
  {
    "type": "put",
    "url": "/api/user/:id",
    "title": "03.Update user",
    "name": "Update_User",
    "group": "01_User",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"id\": \"57e633b94251735cd166f8a9\",\n    \"first_name\": \"abc123456\",\n    \"phone_number\": \"Retail Store\",\n    \"business_type\": \"17.45695\",\n    \"city\": \"18.65515\",\n    \"state\": \"dsgkjhgdj\",\n    \"country\": \"\",\n    \"timezone\": \"\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"user updated successfully\",\n    \"data\": {\n      \"ok\": 1,\n      \"nModified\": 1,\n      \"n\": 1\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/user/controllers/user.js",
    "groupTitle": "01_User"
  },
  {
    "type": "post",
    "url": "/api/display",
    "title": "01.Create new display",
    "name": "Create_Display",
    "group": "02_Display",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"random_key\": \"abc123456\",\n    \"category\": \"Retail Store\",\n    \"latitude\": \"17.45695\",\n    \"longitude\": \"18.65515\",\n    \"display_name\": \"test1\",\n    \"orientation\": \"\",\n    \"location_photo\": \"\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"display registered successfully\",\n    \"data\": {\n      \"__v\": 0,\n      \"user_id\": \"57d844ff59ee01ae39d0881b\",\n      \"device_id\": \"57e5765ed6f24d31a38cf0e5\",\n      \"category\": \"Retail Store\",\n      \"latitude\": \"17.45695\",\n      \"longitude\": \"18.65515\",\n      \"display_name\": \"test1\",\n      \"orientation\": \"1024x768\",\n      \"random_key\": \"abc123456\",\n      \"_id\": \"57e634c80891365cd7923dd1\",\n      \"updated_at\": \"2016-09-24T08:09:44.189Z\",\n      \"created_at\": \"2016-09-24T08:09:44.189Z\",\n      \"status\": \"active\",\n      \"device_sync\": \"false\"\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/display/controllers/display.js",
    "groupTitle": "02_Display"
  },
  {
    "type": "delete",
    "url": "/api/display/:id",
    "title": "04.Delete display",
    "name": "Delete_Display",
    "group": "02_Display",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"display deleted successfully\",\n    \"data\": {\n      \"ok\": 1,\n      \"nModified\": 1,\n      \"n\": 1\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/display/controllers/display.js",
    "groupTitle": "02_Display"
  },
  {
    "type": "get",
    "url": "/api/display",
    "title": "03.Get displays",
    "name": "Get_Displays",
    "group": "02_Display",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort by ['created_at', 'a-z'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Filter category (array) ['hospitals','hotels']</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Filter by status ['active','inactive','disconnected'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Page limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "skip",
            "description": "<p>Page number.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"display updated successfully\",\n    \"data\":  [\n        {\n          \"_id\": \"57e633b94251735cd166f8a9\",\n          \"user_id\": \"57d844ff59ee01ae39d0881b\",\n          \"device_id\": \"57e5765ed6f24d31a38cf0e5\",\n          \"category\": \"Retail Store\",\n          \"latitude\": \"17.45695\",\n          \"longitude\": \"18.65515\",\n          \"display_name\": \"dsgkjhgdj\",\n          \"orientation\": \"1024x768\",\n          \"random_key\": \"abc123456\",\n          \"__v\": 0,\n          \"updated_at\": \"2016-09-24T08:05:13.488Z\",\n          \"created_at\": \"2016-09-24T08:05:13.488Z\",\n          \"status\": \"active\",\n          \"device_sync\": \"false\"\n        }\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/display/controllers/display.js",
    "groupTitle": "02_Display"
  },
  {
    "type": "put",
    "url": "/api/display",
    "title": "02.Update display",
    "name": "Update_Display",
    "group": "02_Display",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"id\": \"57e633b94251735cd166f8a9\",\n    \"random_key\": \"abc123456\",\n    \"category\": \"Retail Store\",\n    \"latitude\": \"17.45695\",\n    \"longitude\": \"18.65515\",\n    \"display_name\": \"dsgkjhgdj\",\n    \"orientation\": \"\",\n    \"location_photo\": \"\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"display updated successfully\",\n    \"data\": {\n      \"ok\": 1,\n      \"nModified\": 1,\n      \"n\": 1\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/display/controllers/display.js",
    "groupTitle": "02_Display"
  },
  {
    "type": "post",
    "url": "/api/widget",
    "title": "01.Create new widget",
    "name": "Create_Widget",
    "group": "04_Widget",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"widget_name\": \"abc123456\",\n    \"type\": \"text\",\n    \"embed_code\": \"\",\n    \"settings\": \"object\",\n    \"thumbnail_url\": \"test1\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"widget created successfully\",\n    \"data\": {\n      \"__v\": 0,\n      \"thumbnail_url\": \"test1\",\n      \"settings\": \"object\",\n      \"embed_code\": \"\",\n      \"type\": \"text\",\n      \"widget_name\": \"abc123456\",\n      \"_id\": \"57e66297e028361b3cf463df\",\n      \"updated_at\": \"2016-09-24T11:25:11.888Z\",\n      \"created_at\": \"2016-09-24T11:25:11.888Z\",\n      \"status\": \"active\"\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/widget/controllers/widget.js",
    "groupTitle": "04_Widget"
  },
  {
    "type": "delete",
    "url": "/api/widget/:id",
    "title": "04.Delete widget",
    "name": "Delete_Widget",
    "group": "04_Widget",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"widget deleted successfully\",\n    \"data\": {\n      \"ok\": 1,\n      \"nModified\": 1,\n      \"n\": 1\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/widget/controllers/widget.js",
    "groupTitle": "04_Widget"
  },
  {
    "type": "get",
    "url": "/api/widget",
    "title": "03.Get widgets",
    "name": "Get_Widgets",
    "group": "04_Widget",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort by ['created_at', 'a-z'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Widget type (array) ['text','video', 'web']</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Filter by status ['active','inactive'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Page limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "skip",
            "description": "<p>Page number.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"widgets fetched successfully\",\n    \"data\":  [\n        {\n          \"_id\": \"57ee8259361b110ce8cafa79\",\n          \"thumbnail_url\": \"\",\n          \"settings\": {\n            \"speed\": 10,\n            \"effect\": \"\",\n            \"videoSourceType\": \"\",\n            \"direction\": \"left\",\n            \"delay\": \"\"\n          },\n          \"embed_code\": \"Text widget\",\n          \"type\": \"text\",\n          \"widget_name\": \"Widget name\",\n          \"__v\": 0,\n          \"updated_at\": \"2016-09-30T15:18:49.001Z\",\n          \"created_at\": \"2016-09-30T15:18:49.001Z\",\n          \"status\": \"active\"\n        }\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/widget/controllers/widget.js",
    "groupTitle": "04_Widget"
  },
  {
    "type": "put",
    "url": "/api/widget",
    "title": "02.Update widget",
    "name": "Update_Widget",
    "group": "04_Widget",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"id\": \"57e66297e028361b3cf463df\",\n    \"widget_name\": \"sdfsdfsd\",\n    \"type\": \"web\",\n    \"embed_code\": \"\",\n    \"settings\": \"\",\n    \"thumbnail_url\": \"test1\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"widget updated successfully\",\n    \"data\": {\n      \"ok\": 1,\n      \"nModified\": 1,\n      \"n\": 1\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/widget/controllers/widget.js",
    "groupTitle": "04_Widget"
  },
  {
    "type": "post",
    "url": "/api/campaign",
    "title": "01.Create new campaign",
    "name": "Create_Campaign",
    "group": "05_Campaign",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"campaign_name\": \"abc123456\",\n    \"play_list\": [{},{}],\n    \"scheduled_at\": \"2016-09-29 05:30:56\",\n    \"thumbnail_url\": \"test1\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"campaign created successfully\",\n    \"data\": {\n      \"__v\": 0,\n      \"thumbnail_url\": \"test1\",\n      \"play_list\": [{},{}],\n      \"scheduled_at\": \"2016-09-29 05:30:56\",\n      \"campaign_name\": \"abc123456\",\n      \"_id\": \"57e66297e028361b3cf463df\",\n      \"updated_at\": \"2016-09-24T11:25:11.888Z\",\n      \"created_at\": \"2016-09-24T11:25:11.888Z\",\n      \"status\": \"active\"\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/campaign/controllers/campaign.js",
    "groupTitle": "05_Campaign"
  },
  {
    "type": "delete",
    "url": "/api/campaign/:id",
    "title": "04.Delete campaign",
    "name": "Delete_Campaign",
    "group": "05_Campaign",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"campaign deleted successfully\",\n    \"data\": {\n      \"ok\": 1,\n      \"nModified\": 1,\n      \"n\": 1\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/campaign/controllers/campaign.js",
    "groupTitle": "05_Campaign"
  },
  {
    "type": "get",
    "url": "/api/campaign",
    "title": "03.Get campaigns",
    "name": "Get_Campaigns",
    "group": "05_Campaign",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort by ['created_at', 'a-z'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Filter by status ['active','inactive'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Page limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "skip",
            "description": "<p>Page number.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"campaigns fetched successfully\",\n    \"data\":  [\n              {\n              \"_id\": \"57ef9509ed98232e2fc8b419\",\n              \"total_duration\": \"0:00:40\",\n              \"campaign_name\": \"new campaign\",\n              \"__v\": 0,\n              \"updated_at\": \"2016-10-01T10:50:49.349Z\",\n              \"created_at\": \"2016-10-01T10:50:49.349Z\",\n              \"status\": \"active\",\n              \"play_list\": [\n                {\n                  \"_id\": \"57ee869e361b110ce8cafa7d\",\n                  \"thumbnail_url\": \"\",\n                  \"settings\": {\n                    \"speed\": \"\",\n                    \"effect\": \"\",\n                    \"videoSourceType\": \"facebook\",\n                    \"direction\": \"left\",\n                    \"delay\": \"\"\n                  },\n                  \"embed_code\": \"facebook.com\",\n                  \"type\": \"video\",\n                  \"widget_name\": \"Video widget\",\n                  \"__v\": 0,\n                  \"updated_at\": \"2016-09-30T15:37:02.623Z\",\n                  \"created_at\": \"2016-09-30T15:37:02.623Z\",\n                  \"status\": \"active\",\n                  \"duration\": \"00:00:10\"\n                }\n              ]\n            }\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/campaign/controllers/campaign.js",
    "groupTitle": "05_Campaign"
  },
  {
    "type": "put",
    "url": "/api/campaign",
    "title": "02.Update campaign",
    "name": "Update_Campaign",
    "group": "05_Campaign",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"id\": \"57e66297e028361b3cf463df\",\n    \"campaign_name\": \"abc123456\",\n    \"play_list\": [{},{}],\n    \"scheduled_at\": \"\",\n    \"thumbnail_url\": \"test1\"\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"campaign updated successfully\",\n    \"data\": {\n      \"ok\": 1,\n      \"nModified\": 1,\n      \"n\": 1\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/campaign/controllers/campaign.js",
    "groupTitle": "05_Campaign"
  },
  {
    "type": "post",
    "url": "/api/scheduler",
    "title": "01.Create new scheduler",
    "name": "Create_Scheduler",
    "group": "06_Scheduler",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    {\n        \"campaign_id\": \"\",\n        \"assets_count\": \"\",\n        \"categories\": [ids,ids] ,\n        \"locations\": [locations,locations],\n        \"displays\": [ids,ids],\n        \"video_duration\": \"\",\n        \"start_time\": \"2016-09-29 00:00:00\",\n        \"end_time\": \"2016-09-29 00:00:00\",\n        \"week_days\": [\"mon\",\"tue\"],\n        \"all_days\": true\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"scheduler created successfully\",\n    \"data\": {\n      \"__v\": 0,\n      \"campaign_id\": \"\",\n      \"assets_count\": \"\",\n      \"categories\": [ids,ids] ,\n      \"locations\": [locations,locations],\n      \"displays\": [ids,ids],\n      \"video_duration\": \"\",\n      \"start_time\": \"00:00:00\",\n      \"end_time\": \"00:00:00\",\n      \"week_days\": [\"mon\",\"tue\"],\n      \"all_days\": true\n      \"_id\": \"57e66297e028361b3cf463df\",\n      \"updated_at\": \"2016-09-24T11:25:11.888Z\",\n      \"created_at\": \"2016-09-24T11:25:11.888Z\",\n      \"status\": \"active\"\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/scheduler/controllers/scheduler.js",
    "groupTitle": "06_Scheduler"
  },
  {
    "type": "delete",
    "url": "/api/scheduler/:id",
    "title": "04.Delete scheduler",
    "name": "Delete_Scheduler",
    "group": "06_Scheduler",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"scheduler deleted successfully\",\n    \"data\": {\n      \"ok\": 1,\n      \"nModified\": 1,\n      \"n\": 1\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/scheduler/controllers/scheduler.js",
    "groupTitle": "06_Scheduler"
  },
  {
    "type": "get",
    "url": "/api/scheduler",
    "title": "03.Get scheduler",
    "name": "Get_Schedulers",
    "group": "06_Scheduler",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort by ['created_at', 'a-z'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Filter by category ['hospitals','schools'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>Filter by location ['Mumbai','Hyderabad'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Filter by status ['active','inactive'].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>Page limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "skip",
            "description": "<p>Page number.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"schedulers fetched successfully\",\n    \"data\":  [\n              {\n              \"_id\": \"57f27d3b93672430078f988a\",\n              \"campaign_id\": \"57ef9509ed98232e2fc8b419\",\n              \"assets_count\": \"4\",\n              \"video_duration\": \"0:00:40\",\n              \"start_time\": \"2016-10-03 00:00:10\",\n              \"end_time\": \"2016-10-03 00:00:10\",\n              \"user_id\": \"57d844ff59ee01ae39d0881b\",\n              \"__v\": 0,\n              \"updated_at\": \"2016-10-03T15:46:03.364Z\",\n              \"created_at\": \"2016-10-03T15:46:03.364Z\",\n              \"status\": \"active\",\n              \"week_days\": [\n                \"mon\",\n                \"tue\",\n                \"fri\"\n              ],\n              \"displays\": [\n                \"abc123456\",\n                \"abc123456\"\n              ],\n              \"categories\": [\n                \"hospitals\",\n                \"schools\"\n              ],\n              \"locations\": [\n                \"mumbai\",\n                \"hyderabad\"\n              ]\n            }\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/scheduler/controllers/scheduler.js",
    "groupTitle": "06_Scheduler"
  },
  {
    "type": "put",
    "url": "/api/scheduler",
    "title": "02.Update scheduler",
    "name": "Update_Scheduler",
    "group": "06_Scheduler",
    "parameter": {
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"id\": \"57e66297e028361b3cf463df\",\n    \"campaign_id\": \"\",\n    \"assets_count\": \"\",\n    \"categories\": [ids,ids] ,\n    \"locations\": [locations,locations],\n    \"displays\": [ids,ids],\n    \"video_duration\": \"\",\n    \"start_time\": \"00:00:00\",\n    \"end_time\": \"00:00:00\",\n    \"week_days\": [\"mon\",\"tue\"],\n    \"all_days\": true\n  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": true,\n    \"message\": \"scheduler updated successfully\",\n    \"data\": {\n        \"campaign_id\": \"\",\n        \"assets_count\": \"\",\n        \"categories\": [ids,ids] ,\n        \"locations\": [locations,locations],\n        \"displays\": [groups,groups],\n        \"video_duration\": \"\",\n        \"start_time\": \"00:00:00\",\n        \"end_time\": \"00:00:00\",\n        \"week_days\": [\"mon\",\"tue\"],\n        \"all_days\": true\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/scheduler/controllers/scheduler.js",
    "groupTitle": "06_Scheduler"
  }
] });
